﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json.Nodes;

namespace WebApplication6.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AESController : ControllerBase
    {
        [HttpPost("Encrypt")]
        public JsonObject Encrypt(JsonObject pairs)
        {
            JsonObject Response = new JsonObject();
            // 要加密的字串
            string plainText = pairs["plainText"].ToString();
            using (Aes aes = Aes.Create())
            {
                // 设置加密和解密使用的密钥和IV
                string base64Key = "SNE6xHezkUC8RARxtdZguWATwZjR7oHesqwilbWsI1A=";
                byte[] key = Convert.FromBase64String(base64Key);
                string base64IV = "BWfzSFJca4LMMTOgpiC4eQ==";
                byte[] IV = Convert.FromBase64String(base64IV);
                aes.Key = key;
                aes.IV = IV;
                // 设置填充模式为PKCS7
                aes.Padding = PaddingMode.PKCS7;
                // 创建加密器和解密器
                ICryptoTransform encryptor = aes.CreateEncryptor();
                ICryptoTransform decryptor = aes.CreateDecryptor();
                // 加密数据
                byte[] data = Encoding.UTF8.GetBytes(plainText);
                byte[] encrypted = encryptor.TransformFinalBlock(data, 0, data.Length);
                // 解密数据
                byte[] decrypted = decryptor.TransformFinalBlock(encrypted, 0, encrypted.Length);
                // 将解密后的数据转换为字符串
                string decryptedText = Encoding.UTF8.GetString(decrypted);
                // 將加密後的位元組陣列轉換成 Base64 字串
                string encryptedText = Convert.ToBase64String(encrypted);
                Response["encryptedText"] = encryptedText;
                Response["decryptedText"] = decryptedText;
            }
            return Response;
        }
        [HttpPost("getKey")]
        public JsonObject getKey()
        {
            JsonObject Response = new JsonObject();
            using Aes aes = Aes.Create();
            aes.GenerateIV(); // 產生一個隨機的 IV
            aes.GenerateKey(); // 產生一個隨機的 IV
            string base64IV = Convert.ToBase64String(aes.IV);
            string base64Key = Convert.ToBase64String(aes.Key);
            Response["base64IV"] = base64IV;
            Response["base64Key"] = base64Key;
            return Response;
        }
    }
}
