﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApplication6.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        // 1. 宣告 API 路由及相關的參數
        [HttpPost("upload")]
        public async Task<IActionResult> Upload(IFormFile file)
        {
            // 2. 檢查是否有上傳檔案
            if (file == null || file.Length == 0)
                return BadRequest("No file uploaded.");

            // 3. 取得檔案名稱及副檔名
            var fileName = Path.GetFileName(file.FileName);
            var fileExtension = Path.GetExtension(fileName);

            // 4. 檢查檔案類型是否為圖片
            if (file.ContentType.StartsWith("image/") && file.Length < 10 * 1024 * 1024) // 限制檔案大小 10 MB
            {
                // 5. 設定檔案存放的目錄路徑
                var uploadsFolder = Path.Combine("C:\\Users\\NTNB-20\\OneDrive - 新人類資訊科技股份有限公司\\工作資料\\筆記\\C#\\blob", "uploads");

                // 6. 如果目錄不存在，則建立目錄
                if (!Directory.Exists(uploadsFolder))
                    Directory.CreateDirectory(uploadsFolder);

                // 7. 設定檔案的新名稱，以避免檔案名稱重複
                var newFileName = $"{Guid.NewGuid()}{fileExtension}";

                // 8. 設定檔案的完整路徑
                var filePath = Path.Combine(uploadsFolder, newFileName);

                // 9. 將檔案寫入磁碟
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }

                // 10. 回傳上傳成功的訊息及檔案的完整路徑
                return Ok(new { message = "File uploaded successfully.", filePath });
            }
            else
            {
                // 11. 回傳檔案類型不正確的錯誤訊息
                return BadRequest("Invalid file type or file size exceeds 10 MB.");
            }
        }
    }
}
