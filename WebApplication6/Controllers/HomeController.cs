﻿using Microsoft.AspNetCore.Mvc;
using System.Security.Cryptography;
using System.Text.Json.Nodes;
using System;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.DataProtection.KeyManagement;
using System.Diagnostics;

namespace WebApplication6.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HomeControllser : ControllerBase
    {
        private readonly ILogger<HomeControllser> _logger;
        public HomeControllser(ILogger<HomeControllser> logger)
        {
            _logger = logger;
        }
        [HttpPost("EncryptBlob")]
        public async Task<IActionResult> UploadFile()
        {
            var form = await Request.ReadFormAsync();
            var file = form.Files.GetFile("file");
            if (file == null || file.Length == 0)
            {
                return BadRequest("No file is uploaded.");
            }

            string fileName = file.FileName;
            string base64Key = "SNE6xHezkUC8RARxtdZguWATwZjR7oHesqwilbWsI1s=";
            byte[] key = Convert.FromBase64String(base64Key);
            string base64IV = "BWfzSFJca4LMMTOgpiC4eQ==";
            byte[] IV = Convert.FromBase64String(base64IV);
            using var memoryStream = new MemoryStream();
            await file.CopyToAsync(memoryStream);
            memoryStream.Position = 0;
            
            using MemoryStream destinationStream = EncryptBlob2(memoryStream, key, IV);
            return new FileContentResult(destinationStream.ToArray(), "application/octet-stream")
            {
                FileDownloadName = fileName + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ".bin"
            };
        }
        [HttpPost("DecryptBlob")]
        public async Task<IActionResult> DecryptBlobtest()
        {
            var form = await Request.ReadFormAsync();
            var file = form.Files.GetFile("file");
            if (file == null || file.Length == 0)
            {
                return BadRequest("No file is uploaded.");
            }

            string fileName = file.FileName;
            string newFileName = fileName.Replace(".bin", "");
            string base64Key = "SNE6xHezkUC8RARxtdZguWATwZjR7oHesqwilbWsI1s=";
            byte[] key = Convert.FromBase64String(base64Key);
            string base64IV = "BWfzSFJca4LMMTOgpiC4eQ==";
            byte[] IV = Convert.FromBase64String(base64IV);
            using var memoryStream = new MemoryStream();
            await file.CopyToAsync(memoryStream);
            memoryStream.Position = 0;
            
            using MemoryStream destinationStream = DecryptBlob2(memoryStream, key,IV);
            return new FileContentResult(destinationStream.ToArray(), "application/octet-stream")
            {
                FileDownloadName = newFileName 
        };
        }
        [HttpPost("BlobTest", Name = "BlobTest")]
        public string BlobTest()
        {
            string filePath = @"C:\Users\NTNB-20\OneDrive - 新人類資訊科技股份有限公司\工作資料\筆記\C#\blob\31.Overtime Claim Application SA V1.0-nt.xlsx"; // 檔案路徑
            string destinationFilePath = @"C:\Users\NTNB-20\OneDrive - 新人類資訊科技股份有限公司\工作資料\筆記\C#\blob\加密.xlsx"; // 檔案路徑
            string base64Key = "i83aJ1yqIH2HlfUBEpfaigJIx3kLjASuBPLKDtdSeIk=";
            byte[] key = Convert.FromBase64String(base64Key);
            using FileStream FileStream1 = EncryptBlob(filePath, destinationFilePath, key);
            string destinationFilePath2 = @"C:\Users\NTNB-20\OneDrive - 新人類資訊科技股份有限公司\工作資料\筆記\C#\blob\解密.xlsx"; // 檔案路徑
            using FileStream FileStream2 = DecryptBlob(destinationFilePath, destinationFilePath2, key);

            return "ok";
        }
     
        [HttpPost("getkey", Name = "getkey")]
        public string getkey()
        {
            using Aes aes = Aes.Create();
            aes.GenerateIV(); // 產生一個隨機的 IV
            aes.GenerateKey(); // 產生一個隨機的 IV
            string base64IV = Convert.ToBase64String(aes.IV);
            string base64Key = Convert.ToBase64String(aes.Key);
            return $"256-bit key: {base64Key}  256-bit IV: {base64IV}  ";
        }
        public static MemoryStream EncryptBlob2(MemoryStream memoryStream,byte[] key, byte[] IV)
        {
            using Aes aes = Aes.Create();
            aes.Key = key;
            aes.IV = IV;
            using MemoryStream destinationStream = new MemoryStream();
            using CryptoStream cryptoStream = new CryptoStream(destinationStream, aes.CreateEncryptor(), CryptoStreamMode.Write);
            memoryStream.CopyTo(cryptoStream);
            return destinationStream;
        }
        public static FileStream EncryptBlob(string sourceFilePath, string destinationFilePath, byte[] key)
        {
            using Aes aes = Aes.Create();
            aes.Key = key;
            aes.IV = new byte[16];
            using FileStream sourceStream = new FileStream(sourceFilePath, FileMode.Open);
            using FileStream destinationStream = new FileStream(destinationFilePath, FileMode.Create);
            using CryptoStream cryptoStream = new CryptoStream(destinationStream, aes.CreateEncryptor(), CryptoStreamMode.Write);
            sourceStream.CopyTo(cryptoStream);
            return destinationStream;
        }
        // 解密 Blob
        public static MemoryStream DecryptBlob2(MemoryStream memoryStream,byte[] key, byte[] IV)
        {
            using Aes aes = Aes.Create();
            aes.Key = key;
            aes.IV = IV;
            using MemoryStream destinationStream = new MemoryStream();
            using CryptoStream cryptoStream = new CryptoStream(destinationStream, aes.CreateDecryptor(), CryptoStreamMode.Write);
            memoryStream.CopyTo(cryptoStream);
            return destinationStream;
        }
        // 解密 Blob
        public static FileStream DecryptBlob(string sourceFilePath, string destinationFilePath, byte[] key)
        {
            using Aes aes = Aes.Create();
            aes.Key = key;
            aes.IV = new byte[16];
            using FileStream sourceStream = new FileStream(sourceFilePath, FileMode.Open);
            using FileStream destinationStream = new FileStream(destinationFilePath, FileMode.Create);
            using CryptoStream cryptoStream = new CryptoStream(sourceStream, aes.CreateDecryptor(), CryptoStreamMode.Read);
            cryptoStream.CopyTo(destinationStream);
            return destinationStream;
        }
    }
}
